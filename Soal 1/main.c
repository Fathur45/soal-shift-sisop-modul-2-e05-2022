#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <dirent.h>

void make_dir(char* name){
    char *argv[] = {"mkdir", "-p", name, NULL};
    execv("/bin/mkdir", argv);
    exit(0);
}

void download(){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"wget", "--no-check-certificate", "'https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp'", "-O", "Characters", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    } 
    else {
        while ((wait(&status)) > 0);
        char *argv[] = {"wget", "--no-check-certificate", "'https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT'", "-O", "Weapons", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
}

void unzip(){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"unzip", "Characters.zip", "-d", "gacha_gacha", NULL};
        execv("/bin/unzip", argv);
        exit(0);
    } 
    else {
        while ((wait(&status)) > 0);
        char *argv[] = {"unzip", "Weapons.zip", "-d", "gacha_gacha", NULL};
        execv("/bin/unzip", argv);   
        exit(0);
    }
}

typedef struct _item{
    char name[60];
    int rarity;
}item;

int json_file_count(char *path){
    DIR *dir;
    struct dirent *dp;
    int count = 0;

    dir = opendir(path);

    if (dir != NULL)
    {
      while ((dp = readdir (dir)))
      if(strstr(dp->d_name, ".json") != NULL)
        count++;

      (void) closedir (dir);
    } 
    else 
        perror ("Couldn't open the directory");
    
    return count;
}

void object_get(item *temp, char* path){
    FILE *fptr;
    char buffer[5000];

    struct json_object *parsed_json, *name, *rarity;
    fptr = fopen(path, "r");
	fread(buffer, 5000, 1, fptr);
	fclose(fptr);

	parsed_json = json_tokener_parse(buffer);
	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);
	strcpy(temp->name, json_object_get_string(name));
	temp->rarity = json_object_get_int(rarity);
}

void get_item(item* temp, char* dir){
    DIR *dir;
    struct dirent *dp;
    char path[500];
    int index = 0;

    dir = opendir(dir);

    if (dir != NULL)
    {
      while ((dp = readdir (dir)))
      if(strstr(dp->d_name, ".json") != NULL) {
          sprintf(path, "%s/%s", dir, dp->d_name);
          object_get(&temp[index++], path);
      }

      (void) closedir (dir);
    } 
    else 
        perror ("Couldn't open the directory");
}

void execute(){
    pid_t child;
    int status;

    // Download Zip
    child = fork();
    if(child < 0) exit(EXIT_FAILURE);

    if(child == 0){
        download();
    }

    else{
        while ((wait(&status)) > 0);

        // Make Directory "gacha_gacha"
        child = fork();
        if(child < 0)
            exit(EXIT_FAILURE);

        if(child == 0)
            make_dir("gacha_gacha");

        else{
            while ((wait(&status)) > 0);

            // Unzip files in gacha_gacha
            child = fork();
            if (child < 0)
                exit(EXIT_FAILURE);

            if (child == 0)
                unzip();
            
            else {
                while ((wait(&status)) > 0);

                if ((chdir("gacha_gacha")) < 0) // Error Check
                    exit(EXIT_FAILURE);
                
                // 
                item *characters, *weapons, temp;
                FILE *fptr;
                struct tm* local;
                time_t t;

                int primogem = 79000;
                int count = 0;
                int char_count = json_file_count("characters");
                characters = (item*) malloc(sizeof(item) * char_count);

                int weapon_count = json_file_count("weapons");
                weapons = (item*) malloc(sizeof(item) * weapon_count);

                char dirname[20], filename[200], objname[200];

                get_item(characters, "characters");
                get_item(weapons, "weapons");

                while(primogem >= 160){
                    if(count % 90 == 0){
                        child = fork();
                        if (child < 0)
                            exit(EXIT_FAILURE);

                        sprintf(dirname, "total_gacha_%d", count + 90);
                        
                        if (child == 0) {
                            mkdir_custom(dirname);
                            exit(0);
                        }
                        else 
                            while ((wait(&status)) > 0);
                    }

                    if(count % 10 == 0){
                        if(count) fclose(fptr);
                        sleep(1);
                        t = time(NULL);
                        local = localtime(&t);
                        sprintf(filename, "%s/%02d:%02d:%02d_gacha_%d.txt", dirname, local->tm_hour, local->tm_min, local->tm_sec, count + 10);
                        fptr = fopen(filename, "w");
                    }

                    count++;
                    primogem -= 160;

                    if(count & 1){
                        temp = characters[rand()%char_count];
                        sprintf(objname, "%d_characters_%s_%d_%d", count, temp.name, temp.rarity, primogem);
                    }

                    else {
                        temp = weapons[rand()%weapon_count];
                        sprintf(objname, "%d_weapons_%s_%d_%d", count, temp.name, temp.rarity, primogem);
                    }

                    fprintf(fptr, "%s\n", objname);
                }
                fclose(fptr);
            }
        }
    }
}

void zip(){
    char* argv[] = {"zip", "-P", "satuduatiga", "-r", "../not_safe_for_wibu.zip", "total_gacha_90",  "total_gacha_180",  "total_gacha_270",  "total_gacha_360",  "total_gacha_450",  "total_gacha_540", NULL};
    execv("/bin/zip",argv);
    exit(0);
}

void finish(){
    pid_t child;
    int status;
    child = fork();

    if (child < 0) {
        exit(EXIT_FAILURE);
    }

    if (child == 0) {
        zip();
    } 
    
    else {
        while ((wait(&status)) > 0);
        if ((chdir("..")) < 0)
            exit(EXIT_FAILURE);
        
        char *argv[] = {"rm", "-r", "gacha_gacha", NULL};
        execv("/bin/rm", argv);
        exit(0);
    }
}

int main() {
    srand(time(0));
    pid_t pid, sid;
    pid = fork();

    struct tm* _local_time;
    time_t _time;

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }
    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1)
    {
        _time = time(NULL);
        _local_time = localtime(&_time);

        if (_local_time->tm_mday == 30 && _local_time->tm_mon == 2 && _local_time->tm_hour == 4 && _local_time->tm_min == 44)
            execute();
        
        if (_local_time->tm_mday == 30 && _local_time->tm_mon == 2 && _local_time->tm_hour == 7 && _local_time->tm_min == 44)
            finish();
        
        sleep(30);
    }
    return 0;
}

//gcc -o ~/Documents/VsCode/SISOP/Gitlab/soal-shift-sisop-modul-2-e05-2022-main/Soal\ 1/main.c
//gcc ~/Documents/GitHub/soal-shift-sisop-modul-2-e05-2022/Soal\ 1/main.c