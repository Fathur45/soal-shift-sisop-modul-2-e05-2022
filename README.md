# Praktikum SISOP Modul 2 Kelompok E05

```
M Fathurrahman Al Faiz - 5025201023
Burhanudin Rifa Pradana - 5025201191
Marsyavero Charisyah Putra - 5025201122
```

# Soal 1

## Main

`srand` dipakai untuk merandom pemilihan random

```c
#include <time.h>

srand(time(0))
```

Dengan fungsi di bawah kita dapat mengambil waktu lokal pada sistem, dan juga untuk membuat variabel waktu

```c
#include <time.h>

struct tm* local;
time_t time;
```

Error Check dan Penutupan STDIN STDOUT STDERR

```bash
if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }
    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
```

Selanjutnya kita lakukan loop yang cek apakah waktu telah sesuai dengan permintaan soal yaitu 30 Maret Jam 04:44, dimana akan dipanggil fungsi `execute` jika 3 jam telah berlalu yaitu pada jam 07:44 maka akan dipanggil function `finish`

```bash
while (1)
    {
        _time = time(NULL);
        _local_time = localtime(&_time);

        if (_local_time->tm_mday == 30 && _local_time->tm_mon == 2 && _local_time->tm_hour == 4 && _local_time->tm_min == 44)
            execute();
        
        if (_local_time->tm_mday == 30 && _local_time->tm_mon == 2 && _local_time->tm_hour == 7 && _local_time->tm_min == 44)
            finish();
        
        sleep(30);
    }
```

## Execute

### Download

```c
void download(){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"wget", "--no-check-certificate", "'https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp'", "-O", "Characters", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    } 
    else {
        while ((wait(&status)) > 0);
        char *argv[] = {"wget", "--no-check-certificate", "'https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT'", "-O", "Weapons", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
}
```

Disini kita memanggil fungsi sebagai berikut

```bash
wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp' -O Characters
```

Yaitu mendowload file dari suatu drive dengan nama `Characters` dimana karena ini juga memanggil dua buah process maka harus kita fork

### Mkdir

```c
void make_dir(char* name){
    char *argv[] = {"mkdir", "-p", name, NULL};
    execv("/bin/mkdir", argv);
    exit(0);
}
```

Disini kita membuat directory baru dengan input `name`

```bash
mkdir -p name
```

### Unzip

```c
void unzip(){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"unzip", "Characters.zip", "-d", "gacha_gacha", NULL};
        execv("/bin/unzip", argv);
        exit(0);
    } 
    else {
        while ((wait(&status)) > 0);
        char *argv[] = {"unzip", "Weapons.zip", "-d", "gacha_gacha", NULL};
        execv("/bin/unzip", argv);   
        exit(0);
    }
}
```

```bash
unzip "Characters.zip -d gacha_gacha
```

Inti dari function ini adalah sebagai berikut, dimana setelah mendowload zip file tersebut maka akan di unzip kedalam folder `gacha_gacha`

```c
item *characters, *weapons, temp;
FILE *fptr;
struct tm* local;
time_t t;

int primogem = 79000;
int count = 0;
int char_count = json_file_count("characters");
characters = (item*) malloc(sizeof(item) * char_count);

int weapon_count = json_file_count("weapons");
weapons = (item*) malloc(sizeof(item) * weapon_count);

char dirname[20], filename[200], objname[200];

get_item(characters, "characters");
get_item(weapons, "weapons");
```

Disini kita membuat beberapa hal

### Object

Adalah struct yang berisi nama dari suatu objek dan kelangkaan objek tersebut

```c
typedef struct _item{
    char name[60];
    int rarity;
}item;
```

### File Count

```c
#include <dirent.h>

int json_file_count(char *path){
    DIR *dir;
    struct dirent *dp;
    int count = 0;

    dir = opendir(path);

    if (dir != NULL)
    {
      while ((dp = readdir (dir)))
      if(strstr(dp->d_name, ".json") != NULL)
        count++;

      (void) closedir (dir);
    } 
    else 
        perror ("Couldn't open the directory");
    
    return count;
}
```

Method ini digunakan untuk menghitung jumlah file dari suatu directory yang digunakan untuk melihat jumlah file `weapons` dan `characters`

### Get Item

```c
void get_item(item* temp, char* dir){
    DIR *dir;
    struct dirent *dp;
    char path[500];
    int index = 0;

    dir = opendir(dir);

    if (dir != NULL)
    {
      while ((dp = readdir (dir)))
      if(strstr(dp->d_name, ".json") != NULL) {
          sprintf(path, "%s/%s", dir, dp->d_name);
          object_get(&temp[index++], path);
      }

      (void) closedir (dir);
    } else perror ("Couldn't open the directory");
}
```

Method ini berfungsi untuk mengambil file dengan format `json` satu satu. Disini juga ada pemanggilan method `object_get`

### Object Get

```c
void object_get(item *temp, char* path){
    FILE *fptr;
    char buffer[5000];

    struct json_object *parsed_json, *name, *rarity;
    fptr = fopen(path, "r");
	fread(buffer, 5000, 1, fptr);
	fclose(fptr);

	parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

	strcpy(temp->name, json_object_get_string(name));
	temp->rarity = json_object_get_int(rarity);
}
```

Disini kita me

### Pembuatan Text

```c
while(primogem >= 160){
                    if(count % 90 == 0){
                        child = fork();
                        if (child < 0)
                            exit(EXIT_FAILURE);

                        sprintf(dirname, "total_gacha_%d", count + 90);
                        
                        if (child == 0) {
                            mkdir_custom(dirname);
                            exit(0);
                        }
                        else 
                            while ((wait(&status)) > 0);
                    }

                    if(count % 10 == 0){
                        if(count) fclose(fptr);
                        sleep(1);
                        t = time(NULL);
                        local = localtime(&t);
                        sprintf(filename, "%s/%02d:%02d:%02d_gacha_%d.txt", dirname, local->tm_hour, local->tm_min, local->tm_sec, count + 10);
                        fptr = fopen(filename, "w");
                    }

                    count++;
                    primogem -= 160;

                    if(count & 1){
                        temp = characters[rand()%char_count];
                        sprintf(objname, "%d_characters_%s_%d_%d", count, temp.name, temp.rarity, primogem);
                    }

                    else {
                        temp = weapons[rand()%weapon_count];
                        sprintf(objname, "%d_weapons_%s_%d_%d", count, temp.name, temp.rarity, primogem);
                    }

                    fprintf(fptr, "%s\n", objname);
                }
                fclose(fptr);
```

Disini adalah pembuatan File txt melalui penggunaan `sprintf` 

1. Setiap kali pengambilan yang mod 10 maka
    
    Akan dibuat file (txt) yang berisi 10 hasil pengambilan tersebut
    
2. Setiap kali pengambilan yang mod 90 maka
    1. Buat folder baru
    2. Letak semua file txt baru disana
    

## Finish

```c
void zip(){
    char* argv[] = {"zip", "-P", "satuduatiga", "-r", "../not_safe_for_wibu.zip", "total_gacha_90",  "total_gacha_180",  "total_gacha_270",  "total_gacha_360",  "total_gacha_450",  "total_gacha_540", NULL};
    execv("/bin/zip",argv);
    exit(0);
}

void finish(){
    pid_t child;
    int status;
    child = fork();

    if (child < 0) {
        exit(EXIT_FAILURE);
    }

    if (child == 0) {
        zip();
    } 
    
    else {
        while ((wait(&status)) > 0);
        if ((chdir("..")) < 0)
            exit(EXIT_FAILURE);
        
        char *argv[] = {"rm", "-r", "gacha_gacha", NULL};
        execv("/bin/rm", argv);
        exit(0);
    }
}
```

Method pertama disini berbentuk seperti berikut

```bash
zip -P satuduatiga -r ../not_safe_for_wibu.zip total_gacha_90  total_gacha_180 total_gacha_270 total_gacha_360 total_gacha_450 total_gacha_540
```

Dimana dipassword -P `satuduatiga` lalu `-r` dengan nama `not_safe_for_wibu` dengan file 

- total_gacha_90
- total_gacha_180
- total_gacha_270
- total_gacha_360
- total_gacha_450
- total_gacha_540

Setelah itu kita remove folder `gacha_gacha`

```bash
rm -r gacha_gacha
```

# Soal 2
```
 pid_t child_id;

    child_id = fork();
    if (child_id==0){
        char *argv[]={"mkdir", "-p", "shift2/drakor", NULL};
        execv("/bin/mkdir",argv);
        //make drakor directory with shell
    }
```
code ini digunakan untuk membuat dir bernama drakor
<br></br>

```
 while ((wait(&status)) > 0);
        child_id=fork();
        if(child_id==0){
            char *argv[]={"wget","-nd", "-r", "https://docs.google.com/uc?export=download&id=1MdAJqTFS1AQOGjCRwfs_MXH-XLiybygy", "-O", "drakor.zip", NULL};
            execv("/bin/unzip", argv);
        //download drakor.zip
        }
```
code ini digunakan untuk download file drakor.zip dari link
<br></br>

```
 while ((wait(&status)) > 0);
            child_id=fork();
            if(child_id==0){
                char *argv[]={"unzip", "drakor.zip", "*.png", NULL};
                execv("/bin/unzip",argv);

                //unzip drakor.zip send it to the directory that has been made, also make sure to take png 
            }
```
code ini digunakan untuk unzip file drakor yang telah didownload dengan mengambil png saja
<br></br>

```
 while ((wait(&status)) > 0);
                child_id=fork();
                if(child_id==0){
                    char *argv[]={"mkdir", "-p", "shift2/drakor/action", NULL};
                    execv("/bin/mkdir",argv);
                    //make action directory with shell
                }
```
code ini dan code dibawahnya (yang berganti hanya parameternya) digunakan untuk membuat folder genre dalam drakor
<br></br>

```
while ((wait(&status)) > 0);
                                    child_id=fork();
                                    if(child_id==0){
                                        DIR *dp;
                                            struct dirent *ep;
                                            FILE *fptr1, *fptr2;

                                            dp = opendir(".");

                                            if (dp != NULL)
                                            {
                                            while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL) {
                                                char* token1, pngname[3][50], dirname[300], temp, strtemp[280];
                                                int count = 0;
                                                FILE *fptr;
                                                strcpy(strtemp, ep->d_name);

                                                token1 = strtok(strtemp, "_;.");
                                            
                                                while( token1 != NULL ) {
                                                    count++;
                                                    if ((count == 1 || count == 4) && strcmp(token1, "png") != 0){
                                                        strcpy(pngname[0], token1);
                                                    }
                                                    else if (count == 2 || count == 5){
                                                        strcpy(pngname[1], token1);
                                                    }
                                                    else if (count == 3 || count == 6){
                                                        strcpy(pngname[2], token1);
                                                        sprintf(dirname, "%s/%s_%s_%s.png", pngname[2], pngname[1], pngname[0], pngname[2]);
                                                        fptr1 = fopen(ep->d_name, "rb");
                                                        fptr2 = fopen(dirname, "wb");

                                                        while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);

                                                        fclose(fptr1);
                                                        fclose(fptr2);
                                                    }
                                                    
                                                    token1 = strtok(NULL, "_;.");
                                                    }
```
code ini digunakan untuk memindahkan file png drakor ke folder masing masing dengan mengambil bagian substring yang berisi genre(menggunakan strtok dengan delimiter) untuk menentukan folder mana yang dimasuki
<br></br>


```
  DIR *dp;
    struct dirent *ep;
    int index = 0, count, status;
    char poster_name[10][280], temp[280], *token, name[280], year[280], dirname[100], temp2[290], temp3[290];
    FILE *fptr;
    pid_t child_id;

    dp = opendir(dir);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL){
          strcpy(poster_name[index++], ep->d_name);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    for(int i=0; i<index-1; i++)for (int j=0; j<index-i-1; j++){
        if(strcmp(poster_name[j], poster_name[j+1]) > 0){
            strcpy(temp, poster_name[j]);
            strcpy(poster_name[j], poster_name[j+1]);
            strcpy(poster_name[j+1], temp);
        }
    }
    sprintf(dirname, "%s/data.txt", dir);
    fptr = fopen(dirname, "w");
    fprintf(fptr, "kategori : %s\n", dir);
    for(int i=0; i<index; i++) {
        strcpy(temp, poster_name[i]);
        count = 0;
        token = strtok(temp, "_");
        while(token != NULL){
            count++;
            if(count == 1) strcpy(year, token);
            else if (count == 2) strcpy(name, token);
            token = strtok(NULL, "_");
        }
        fprintf(fptr, "\nnama : %s", name);
        fprintf(fptr, "\nrilis : %s\n", year);
        sprintf(temp3, "%s/%s.png", dir, name);
        sprintf(temp2, "%s/%s", dir, poster_name[i]);
        child_id = fork();
        if (child_id < 0) exit(EXIT_FAILURE);

        if (child_id == 0) {
                char *argv[] = {"mv", temp2, temp3, NULL};
                execv("/bin/mv", argv);

        } else while ((wait(&status)) > 0);
    }
    fclose(fptr);
```
code ini digunakan untuk menulis log didalam setiap folder genre dengan membaca seluruh filenya lalu append ke dalam file data.txt berdasarkan format yang telah ditentukan
<br></br>

# Soal 3
## 3.a
pada pengerjaan nomor 3 kami menggunakan beberapa library yaitu sebagai berikut:
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>
```
variabel yang kami gunakan pada program kami adalah:
```bash
pid_t child_id, child_id1, child_id2, child_id3, child_id4, child_id5, child_id6, child_id7;
    int status;
```
Sesuai dengan tutorial [wait x fork x exec](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2#wait-x-fork-x-exec) yang ada di modul 2, untuk membuat folder `darat` dan `air` kami melakukan fork dengan `child_id` dan `child_id1` untuk membuat 2 proses yang membuat folder `darat` dan `air`  dan menambahkan `sleep(3)` untuk membuat jeda 3 detik antar pembuatan folder dengan kode sebagai berikut
```bash
child_id = fork();
    if(child_id < 0) exit(EXIT_FAILURE);

    if(child_id == 0){
        char *argv[] = {"mkdir", "-p", "/home/mcharisyah/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    } else 
        while((wait(&status)) > 0);
        
    child_id1 = fork();
    if(child_id1 < 0) exit(EXIT_FAILURE);

    if(child_id1 == 0){
        sleep(3);
        char *argv[] = {"mkdir", "-p", "/home/mcharisyah/modul2/air", NULL};
        execv("/bin/mkdir", argv);
    } else while((wait(&status)) > 0);
```
## 3b
Untuk melakukan unzip dari file `animal.zip`, kami melakukan fork menggunakan `child_id2` dengan kode sebagai berikut:
```bash
 child_id2 = fork();

    if(child_id2 < 0) exit(EXIT_FAILURE);

    if(child_id2 == 0){
        char *argv[] = {"unzip", "-oq", "/home/mcharisyah/Downloads/animal.zip", "-d", "/home/mcharisyah/modul2/", NULL};
        execv("/usr/bin/unzip", argv);
    } else while((wait(&status)) > 0);
```
- Command untuk unzip disimpan pada variabel argv dengan isi: `"unzip"`, `"-oq"`, `"/home/mcharisyah/Downloads/animal.zip"`, `"-d"`, `"/home/mcharisyah/modul2/"`, `NULL.`

## 3c
Untuk memindahkan file yang mengandung nama darat dan air ke folder masing-masing, digunakan kode sebagai berikut:
```bash
 child_id3 = fork();
    if(child_id3 < 0) exit(EXIT_FAILURE);

    if(child_id3 == 0){
        execl("/usr/bin/find", "find", "/home/mcharisyah/modul2/animal/", "-type", "f", "-name", "*darat*", "-exec", "mv", "-t", "/home/mcharisyah/modul2/darat", "{}", "+", (char *) NULL);
    } else while((wait(&status)) > 0);
                
    child_id4 = fork();
    if(child_id4 < 0) exit(EXIT_FAILURE);

    if(child_id4 == 0){
        sleep(3);
        execl("/usr/bin/find", "find", "/home/mcharisyah/modul2/animal/", "-type", "f", "-name", "*air*", "-exec", "mv", "-t", "/home/mcharisyah/modul2/air/", "{}", "+", (char *) NULL);
    } else while(wait((&status)) > 0);
```
- Untuk memindahkan file, pertama dilakukan `find` terlebih dahulu, terhadap seluruh file yang mengandung `*darat*` dan `*air*`. Setelah itu, dilakukan `"-exec"`, `"mv"`, `"-t"`, `"/home/mcharisyah/modul2/darat"` dan `"-exec"`, `"mv"`, `"-t"`, `"/home/mcharisyah/modul2/air/"` untuk memindahkan file-file tersebut ke directory yang diinginkan.
- Diberikan `sleep(3)` pada pembuatan folder air untuk memberikan jeda sebanyak 3 detik setelah pemindahan isi folder animal.

lalu, untuk mengosongkan direktori animal menggunakan
```bash
 child_id5 = fork();

    if(child_id5 < 0) exit(EXIT_FAILURE);

    if(child_id5 == 0) 
        execl("/bin/sh", "sh", "-c", "rm -f /home/mcharisyah/modul2/animal/*", (char *) NULL);
    else while(wait((&status)) > 0);
```
## 3d

Untuk menghapus file yang memiliki nama **bird** pada folder darat, digunakan kode sebagai berikut:
```bash
child_id6 = fork();
    if(child_id6 < 0) exit(EXIT_FAILURE);

    if (child_id6 == 0){
        execl("/bin/sh", "sh", "-c", "rm -f /home/mcharisyah/modul2/darat/*bird*", (char *) NULL);
    }   
```
- `rm` adalah perintah dalam untuk melakukan remove
- `-f` adalah argument yang memaksa menghapus tanpa mengeluarkan prompt

## 3e

Untuk memasukkan list nama file ke dalam file `list.txt` dengan format yang diberikan **(UID_UID Permission_Filename)**, kami menggunakan kode sebagai berikut:
```bash
child_id7 = fork();
    if(child_id7 < 0) exit(EXIT_FAILURE);
  
    if (child_id7 == 0){
        execl("/bin/sh", "sh", "-c", "ls -la /home/mcharisyah/modul2/air | awk 'NR > 3 {print $3\"_\"substr($1,2,2)\"_\"$9}' > /home/mcharisyah/modul2/air/list.txt", (char *) NULL);
    }   
    
    else while(wait((&status)) > 0);
}
```
- `sh` untuk menjalankan command pada shell

- `ls -la /home/mcharisyah/modul2/air` untuk mendapatkan list detail seluruh permission dan nama user yang terdapat dalam `/home/mcharisyah/modul2/air`

Selanjutnya untuk mengolah data pada `ls -la`, digunakan `awk` untuk melakukan print dan memasukkan ke dalamm `list.txt`.
command yang digunakan dalam `awk` sendiri adalah `{print $3\"_\"substr($1,2,2)\"_\"$9}`